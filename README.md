# Pong

![Pong](pong-screenshot.png)

A simple standard implementation of the game Pong in the Godot engine. The game was created in connection with a [tutorial series](https://youtube.com/playlist?list=PLiBLFJ8tU-vWSz88caSb3WqXagnp62yPn) on YouTube.

Features:
- 100% GDScript
- Simple ball following ai
- Speed boost at keypress
